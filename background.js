const cookieName = "doyotatest";
const targetValue = "true";
const unixYear2025 = 1735689600;

const changeHandler = async () => {
  console.log("BG function");

  const url = await getUrl();

  chrome.permissions.contains(
    { permissions: ["cookies"], origins: [url] },
    (allowed) => {
      if (allowed) {
        console.log("allowed");
        chrome.action.enable();
        checkAndSetFlag();
      } else {
        chrome.action.setIcon({
          path: "icons/icon-init.png",
        });
        chrome.action.disable();
        console.log("not allowed");
      }
    }
  );
};

const checkAndSetFlag = async () => {
  const flagValue = await getCookie(cookieName);
  const url = await getUrl();

  chrome.storage.sync.get({ value: true }, async (items) => {
    const toggle = items.value;

    if (toggle && flagValue !== targetValue) {
      console.log("set");
      chrome.cookies.set({
        url,
        name: cookieName,
        expirationDate: unixYear2025,
        value: targetValue,
        path: "/",
      });
    }

    setIcon(toggle, flagValue === targetValue);
  });
};

const getCookie = async (name) => {
  const url = await getUrl();

  const cookie = await chrome.cookies.get({
    url,
    name,
  });

  return cookie ? cookie.value : undefined;
};

const getTab = async () => {
  const [tab] = await chrome.tabs.query({
    active: true,
    currentWindow: true,
  });

  return tab;
};

const getUrl = async () => {
  const tab = await getTab();

  return tab.url;
};

const setIcon = (toggle, flag) => {
  let path = "icons/icon-init.png";
  if (toggle && flag) {
    path = "icons/icon-tt.png";
  }
  if (toggle && !flag) {
    path = "icons/icon-tf.png";
  }
  if (!toggle && flag) {
    path = "icons/icon-ft.png";
  }
  if (!toggle && !flag) {
    path = "icons/icon-ff.png";
  }

  chrome.action.setIcon({
    path,
  });
};

chrome.cookies.onChanged.addListener(changeHandler);

chrome.storage.onChanged.addListener(changeHandler);

chrome.tabs.onActivated.addListener(changeHandler);

chrome.tabs.onUpdated.addListener(changeHandler);
