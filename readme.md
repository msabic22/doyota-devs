# Doyota Devs Chrome Extension

This extension is used to control **doyotatest** cookie.

When the extension is active, the cookie will always be set to "true".

# Installation

1. Clone the repository

2. Open chrome extensions : [chrome://extensions/](chrome://extensions/)

3. Find **Developer mode** option and toggle it **ON**

4. Click **load unpacked** and select the folder of cloned repository

5. You should see **Doyota Devs** extension added

6. Click **Details** in the Doyota Devs extension card

7. FInd **Allow in Incognito** option and toggle it **ON**

8. Thats it!

## Usage

When installed, the extension will work quietly in the background and always keep the doyotatest cookie set to true.

If want you want extra control over the extension, you can pin the extension to the right of the chrome search bar by clicking the puzzle icon, finding Doyota Devs and selecting the pin icon.

## Popup

When the extension is pinned, the icon with toyota logo will be displayed to the right of the chrome search bar. When the icon is clicked the popup with a few options will be shown :

- **Extension Active** - You can disable the extension if you want to delete the cookie.
- **Flag Value** - Shows current flag (cookie) value
- **Reset Flag** - Sets the flag back to true
- **Delete Flag** - If you want to delete the flag, you need to disable the extension first

## Extension status

Extension status is also visible in the popup icon.

There are 2 circles shown in the icon, the left one indicates if the extension is active and the right one indicates if the flag is set to "true"

| State                        | Icon                                                       |
| ---------------------------- | ---------------------------------------------------------- |
| Extension initialisation     | ![Extension initialisation](icons/icon-init.png "Title")   |
| Extension ON - Flag SET      | ![Extension ON - Flag SET](icons/icon-tt.png "Title")      |
| Extension ON - Flag NOT SET  | ![Extension ON - Flag NOT SET](icons/icon-tf.png "Title")  |
| Extension OFF - Flag SET     | ![Extension OFF - Flag SET](icons/icon-ft.png "Title")     |
| Extension OFF - Flag NOT SET | ![Extension OFF - Flag NOT SET](icons/icon-ff.png "Title") |

## Known bugs

- Doesnt work in incognito mode
