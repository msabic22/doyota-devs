const statusSpan = document.getElementById("status");
const onOffSwitch = document.getElementById("on-off-switch");
const deleteButton = document.getElementById("delete");
const resetButton = document.getElementById("reset");
const cookieName = "doyotatest";
const targetValue = "true";
const unixYear2025 = 1735689600;

const onLoad = async () => {
  console.log("popup load");
  restoreOptions();
  onOffSwitch.addEventListener("click", toggleOnOff);
  const flagValue = await getCookie(cookieName);
  statusSpan.innerHTML = flagValue;
};

const deleteCookie = async () => {
  const url = await getUrl();

  chrome.cookies.remove({ url, name: cookieName });
};

const resetCookie = async () => {
  const url = await getUrl();

  chrome.cookies.set({ url, name: cookieName, value: targetValue });
};

const getCookie = async (name) => {
  const url = await getUrl();

  const cookie = await chrome.cookies.get({
    url,
    name,
  });

  return cookie ? cookie.value : undefined;
};

const getUrl = async () => {
  const [urlObject] = await chrome.tabs.query({
    active: true,
    currentWindow: true,
  });

  return urlObject.url;
};

function toggleOnOff() {
  // onOffSwitch.checked = !onOffSwitch.checked;
  const switchState = onOffSwitch.checked;
  chrome.storage.sync.set(
    {
      value: switchState,
    },
    function () {}
  );
}

function restoreOptions() {
  // Use default value = false.
  chrome.storage.sync.get(
    {
      value: false,
    },
    function (items) {
      onOffSwitch.checked = items.value;
    }
  );
}

const checkAndSetFlag = async () => {
  console.log("popup check and set");
  const flagValue = await getCookie(cookieName);
  statusSpan.innerHTML = flagValue;
};

document.addEventListener("DOMContentLoaded", onLoad);

chrome.storage.onChanged.addListener(checkAndSetFlag);

chrome.cookies.onChanged.addListener(checkAndSetFlag);

deleteButton.addEventListener("click", deleteCookie);

resetButton.addEventListener("click", resetCookie);
